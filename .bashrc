alias emacs="/Applications/Emacs.app/Contents/MacOS/Emacs -nw"
alias ls="ls -la"
alias rm="rm -i"
if [[ -x `which colordiff` ]]; then
    alias diff='colordiff -u'
else
    alias diff='diff -u'
fi
type direnv > /dev/null 2>&1 && eval "$(direnv hook bash)"
[[ -s $HOME/.pythonz/etc/bashrc ]] && source $HOME/.pythonz/etc/bashrc
