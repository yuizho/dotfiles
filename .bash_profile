export PATH=/usr/local/bin:$PATH
export EDITOR="/usr/bin/emacs"
### Added by the Heroku Toolbelt
export PATH="/usr/local/heroku/bin:$PATH"
export ENV=LOCAL_ENV
test -r ~/.bashrc && . ~/.bashrc
